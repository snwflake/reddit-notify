const growl = require('./lib/cgrowl');
const snoowrap = require('snoowrap');
const cfg = require('./credentials.json');
const msgUrl = 'https://www.reddit.com/message/messages/';
const req = new snoowrap({
  userAgent: cfg.userAgent,
  clientId: cfg.clientId,
  clientSecret: cfg.clientSecret,
  username: cfg.username,
  password: cfg.passwd
});

function exec(){
  var d = new Date();
  var sec = Math.floor(d.getTime() / 1000);

  // GET UNREAD MESSAGES \\
  var msgs = req.getUnreadMessages();
  msgs.forEach((msg) => {
    if(!msg.was_comment){
      notify(
        'message',
        msg.author.name,
        msg.subject + ' ' + msg.body_html
          .replace(/<[^>]*>/g, '')
          .replace(/\r?\n|\r/g, ' '),
        msgUrl + msg.id
      );
    }
  });

  // GET UNREAD POSTS \\
  var posts = req.getNew(cfg.subreddit, {
    "limit": 5
  });
  posts.forEach((post) => {
    if(post.created_utc >= sec - 62 && post.author.name !== cfg.username){
      notify(
        'post',
        post.author.name,
        post.selftext.replace(/(?:https?|ftp):\/\/[\n\S]+/g, ''),
        post.url
      );
    }
  });

  // GET UNREAD COMMENTS \\
  var comments = req.getSubreddit(cfg.subreddit).getNewComments({
    "limit": 10
  });
  comments.forEach((comment) => {
    if(comment.created_utc >= sec - 60 && comment.author.name !== cfg.username){
      notify(
        'comment',
        comment.author.name,
        comment.body
	  .replace(/(?:https?|ftp):\/\/[\n\S]+/g, '')
	  .replace(/\r?\n|\r/g, ' '),
        comment.link_url + comment.id
      );
    }
  });
}

function notify(context, author, body, url){
  growl(author + ': ' + body, {
    title: 'New ' + context + '!',
    name: 'Reddit',
    nType: 'Reddit',
    url: url,
    image: __dirname + '\\img\\' + context + '.png'
  });
}

// exec();
setInterval(exec, 60000);
